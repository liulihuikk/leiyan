// import jweixin from 'jweixin-module'
import config from '@/common/config.js'
import jweixin from 'jweixin-module'
import { getShareParam } from '@/common/request.js'

export default {
	//判断是否在微信中  
	isWechat() {
		var ua = window.navigator.userAgent.toLowerCase();
		if (ua.match(/micromessenger/i) == 'micromessenger') {
			// console.log(‘是微信客户端‘)
			return true;
		} else {
			// console.log(‘不是微信客户端‘)
			return false;
		}
	},
	initJssdk(url) {
		// if (!this.isWechat()) {
		// 	return
		// }
		return new Promise((resolve, reject) => {
			const UA = window.navigator.userAgent.toLowerCase()
			const isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA))
			let _url = url
			if (isIOS) {
				// 如果是单页应用, 针对ios需要单独处理
				// ios只需要#前面的域名, 而安卓需要完整的url
			}
			console.log('_url', _url);
			getShareParam(_url).then((res) => {
				jweixin.config({
					debug: false,
					appId: res.result.appId,
					timestamp: res.result.timestamp,
					nonceStr: res.result.nonceStr,
					signature: res.result.signature,
					jsApiList: [
						'checkJsApi',
						'onMenuShareTimeline',
						'onMenuShareAppMessage'
					]
				})
				jweixin.ready(() => {
					resolve()
				})
			})
		})
	},
	//在需要自定义分享的页面中调用  
	share(url, config = {}) {
		this.initJssdk(url).then((res) => {
			var shareData = {
				title: config.title,
				desc: config.desc,
				link: config.link,
				imgUrl: config.imgUrl,
				success: function(res) {
					//用户点击分享后的回调，这里可以进行统计，例如分享送金币之类的  
				},
				cancel: function(res) {}
			};
			//分享给朋友接口
			jweixin.updateAppMessageShareData(shareData);
			//分享到朋友圈接口  
			jweixin.updateTimelineShareData(shareData);
		})
	}
}
