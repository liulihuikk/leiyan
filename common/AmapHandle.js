import AMapJS from "amap-js";

var aMapJSAPILoader = new AMapJS.AMapJSAPILoader({
	key: "366fa30268423cbac363e421bba7c7b0",
	v: "1.4.15", // 版本号
	params: {
		plugin: 'AMap.Geocoder,AMap.CitySearch'
	}, // 请求参数
	protocol: "https:" // 请求协议
});
var Amap = null


const getLocalCity = async () => {
	return new Promise((resolve, reject) => {
		aMapJSAPILoader.load().then((AMap) => {
			if (!Amap) {
				Amap = new AMap.Map("icneter", {
					resizeEnable: true
				})
			}
			Amap.plugin('AMap.CitySearch', () => {
				const CitySearch = AMap.CitySearch
				const citySearch = new CitySearch();
				Amap.addControl(citySearch);
				citySearch.getLocalCity((status, result) => {
					console.log('getLocalCity', result)
					resolve(result)
				})
			})
		})
	})
}

const getLatAndLng = (address) => {
	var geocoder, marker
	return new Promise((resolve, reject) => {
		aMapJSAPILoader.load().then((AMap) => {
			if (!Amap) {
				Amap = new AMap.Map("icneter", {
					resizeEnable: true
				})
			}
			
			if(!geocoder){
				geocoder = new AMap.Geocoder({});
			}
			
			geocoder.getLocation(address, function(status, result) {
				if (status === 'complete') {
					resolve(result.geocodes[0].location)
				} else {
					resolve({})
				}
			});
		})
	})
}

// 	var geocoder,marker
// 
// 	function geoCode(data) {
// 		if(!geocoder){
// 			geocoder = new AMap.Geocoder({});
// 		}
// 		var address  = data
// 		geocoder.getLocation(address, function(status, result) {
// 			console.log(status);
// 			console.log(result);
// 			if (status === 'complete') {
// 				uni.$emit('gotGeocoder', result.geocodes[0].location)
// 			} else {
// 				uni.$emit('gotGeocoder', {})
// 			}
// 
// 			// 	var lnglat = result.geocodes[0].location
// 			// 	if(!marker){
// 			// 		marker = new AMap.Marker();
// 			// 		Amap.add(marker);
// 			// 	}
// 			// 	marker.setPosition(lnglat);
// 			// 	Amap.setFitView(marker);
// 			// }else{
// 			// 	log.error('根据地址查询位置失败');
// 			// }
// 		});
// 	}

export default {
	getLocalCity,
	getLatAndLng
}