const config = {
	preventRepeatLocation: true ,// 预防一个页面，并发多个请求，多次跳往登录页。记录状态用
	qqmapWxSdkKey: 'CXGBZ-BPHWD-AQI4K-HHT3O-YBXA7-MRB7Z',
	qqmapH5SdkKey: '4WTBZ-52AHW-JCDR5-O34QO-YTAFT-MGBAE',
	amapH5SdkKey: '366fa30268423cbac363e421bba7c7b0'
}
let origin = ''
// #ifdef H5
origin = location.origin
config.host = process.env.NODE_ENV === 'development' ? '/api' : `${origin}/api`
// config.host = process.env.NODE_ENV === 'development' ? '' : `${origin}/api`
// #endif


// #ifdef MP-WEIXIN
config.host = 'https://daojia.gray.motivape.cn/api'
// config.host = 'http://test.daojia.motivape.cn/api'
// config.host = 'https://daojia.motivape.cn/api'
// config.host = 'http://192.168.10.53:9091'
// #endif

// #ifdef APP-PLUS
config.host = 'https://daojia.gray.motivape.cn/api'
// config.host = 'https://daojia.motivape.cn/api'
// config.host = 'http://192.168.10.108:9091'
// #endif

export default config

/*
MOTI测试环境
m端商城：test.mall.motivape.cn
m端商城api：http://test.mall.motivape.cn/api/...
注册中心：test.eureka.motivape.cn
swagger
	h5商城——http://39.96.191.19:8090/swagger-ui.html
	支付中心——http://39.96.191.19:8092/swagger-ui.html
	用户中心——http://39.96.191.19:8096/swagger-ui.html
	订单中心——http://39.96.191.19:8098/swagger-ui.html

MOTI生产环境
m端商城：mall.motivape.cn
	mall.gray.motivape.cn
	mall.prod.motivape.cn
m端商城api：mall.motivape.cn/api
注册中心：eureka.motivape.cn
	eureka.gray.motivape.cn
	eureka.prod.motivape.cn
*/
