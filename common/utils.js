import cityData from '@/common/city-data/city.js'
import store from '@/store/index.js'


	
export function formatTime(time) {
	if (typeof time !== 'number' || time < 0) {
		return time
	}

	var hour = parseInt(time / 3600)
	time = time % 3600
	var minute = parseInt(time / 60)
	time = time % 60
	var second = time

	return ([hour, minute, second]).map(function(n) {
		n = n.toString()
		return n[1] ? n : '0' + n
	}).join(':')
}

export function formatLocation(longitude, latitude) {
	if (typeof longitude === 'string' && typeof latitude === 'string') {
		longitude = parseFloat(longitude)
		latitude = parseFloat(latitude)
	}

	longitude = longitude.toFixed(2)
	latitude = latitude.toFixed(2)

	return {
		longitude: longitude.toString().split('.'),
		latitude: latitude.toString().split('.')
	}
}
export const dateUtils = {
	UNITS: {
		'年': 31557600000,
		'月': 2629800000,
		'天': 86400000,
		'小时': 3600000,
		'分钟': 60000,
		'秒': 1000
	},
	humanize: function(milliseconds) {
		var humanize = '';
		for (var key in this.UNITS) {
			if (milliseconds >= this.UNITS[key]) {
				humanize = Math.floor(milliseconds / this.UNITS[key]) + key + '前';
				break;
			}
		}
		return humanize || '刚刚';
	},
	format: function(dateStr) {
		var date = this.parse(dateStr)
		var diff = Date.now() - date.getTime();
		if (diff < this.UNITS['天']) {
			return this.humanize(diff);
		}
		var _format = function(number) {
			return (number < 10 ? ('0' + number) : number);
		};
		return date.getFullYear() + '/' + _format(date.getMonth() + 1) + '/' + _format(date.getDay()) + '-' +
			_format(date.getHours()) + ':' + _format(date.getMinutes());
	},
	parse: function(str) { //将"yyyy-mm-dd HH:MM:ss"格式的字符串，转化为一个Date对象
		var a = str.split(/[^0-9]/);
		return new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
	}
};

export function checkMobile(mobile) {
	let reg = /^1[3456789]\d{9}$/
	return reg.test(mobile)
}

export function checkLogin() {
	// #ifdef H5
	var arr, reg = new RegExp("(^| )" + 'token' + "=([^;]*)(;|$)")
	if (arr = document.cookie.match(reg)) return unescape(arr[2])
	else return false
	// #endif
	
	// #ifdef MP-WEIXIN
	return false
	// #endif
}
export async function isLogin() {
	// const isLogin = store.getters.isLogin
	// if (isLogin) {
	// 	return true
	// } else {
	// 	const res = new Promise((resolve, reject) => {
	// 		// #ifdef H5
	// 		var arr, reg = new RegExp("(^| )" + 'token' + "=([^;]*)(;|$)")
	// 		const res = !!document.cookie.match(reg)
	// 		resolve(res)
	// 		// #endif
	// 		
	// 		// #ifdef MP-WEIXIN
	// 		wx.checkSession({
	// 			success(res) {
	// 				console.log('checkSession success', res);
	// 				resolve(true)
	// 			},
	// 			fail(err) {
	// 				console.log('checkSession fail', err);
	// 				wx.login({
	// 					success(res) {
	// 						console.log('login success', res);
	// 						resolve(true)
	// 					},
	// 					fail(err) {
	// 						console.log('login fail', err);
	// 						resolve(true)
	// 					}
	// 				})
	// 			}
	// 		})
	// 		// #endif
	// 	})
	// }
	
	// console.log('isLogin', res);
}


export function getCookie(name) {
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)")
	if (arr = document.cookie.match(reg)) return unescape(arr[2])
	else return null;
}
export function isWxBrowser() { // H5方面判断是否是微信浏览器
	let ua = navigator.userAgent.toLowerCase()
	if (ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true
	} else {
		return false
	}
}

export function formatDate(dt, fmt) {
	const d = new Date(dt);
	const o = {
		'M+': d.getMonth() + 1, // 月份
		'D+': d.getDate(), // 日
		'H+': d.getHours(), // 小时
		'm+': d.getMinutes(), // 分
		's+': d.getSeconds(), // 秒
		'q+': Math.floor((d.getMonth() + 3) / 3), // 季度
		'S+': d.getMilliseconds(), // 毫秒
	};
	if (/(Y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (d.getFullYear() + '').substr(4 - RegExp.$1.length));
	for (const k in o) {
		if (new RegExp(`(${k})`).test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
		}
	}
	return fmt;
}

// 多维数组展开
export function flatten(arr) {
	let res = []
	for (var i = 0; i < arr.length; i++) {
		if (Array.isArray(arr[i])) {
			res = res.concat(flatten(arr[i]));
		} else {
			res.push(arr[i]);
		}
	}
	return res;
}

const cartCache = {
	cart: {
		// getCar几口返回的数据格式
		hourItems: {},
		averageItems: {},
		items: {}
	},
	getCartCache() {
		this.cart = uni.getStorageSync('cart_cache') || {
			hourItems: {},
			averageItems: {},
			items: {}
		}
		return this.cart
	},
	getCartCacheNum() {
		const cart = this.getCartCache()
		return Object.keys(cart.items).length
	},
	addCartCache(goods) {
		if (goods.sendType === 'hour') {
			for (let item in this.cart.hourItems) {
				if (item === goods.skuId) {
					goods.skuNum = Number(this.cart.hourItems[item].skuNum) + Number(goods.skuNum)
					break
				}
			}
			this.cart.hourItems[goods.skuId] = goods
			this.cart.items[goods.skuId] = goods
		} else if (goods.sendType === 'average') {
			for (let item in this.cart.averageItems) {
				if (item === goods.skuId) {
					goods.skuNum = Number(this.cart.averageItems[item].skuNum) + Number(goods.skuNum)
					break
				}
			}
			this.cart.averageItems[goods.skuId] = goods
			this.cart.items[goods.skuId] = goods
		} else {
			for (let item in this.cart.items) {
				if (item === goods.skuId) {
					goods.skuNum = Number(this.cart.averageItems[item].skuNum) + Number(goods.skuNum)
					break
				}
			}
			this.cart.items[goods.skuId] = goods
		}
		uni.setStorageSync('cart_cache', this.cart)

	},
	delCartCache(skuId, type) {
		if (type === 'hour') {
			for (let item in this.cart.hourItems) {
				if (item === skuId) {
					delete this.cart.hourItems[skuId]
					delete this.cart.items[skuId]
				}
			}
		} else if (type === 'average') {
			for (let item in this.cart.averageItems) {
				if (item === skuId) {
					delete this.cart.averageItems[skuId]
					delete this.cart.items[skuId]
				}
			}
		} else {
			for (let item in this.cart.items) {
				if (item === skuId) {
					delete this.cart.items[skuId]
				}
			}
		}
		uni.setStorageSync('cart_cache', this.cart)
	},
	clearCartCache() {
		uni.removeStorageSync('cart_cache');
		this.cart = {
			// getCar几口返回的数据格式
			hourItems: {},
			averageItems: {},
			items: {}
		}
	}
}

// 调起微信公众号支付
export function doWXplatformPay(data) {

	// 以下代码是生成二次签名用，时间戳可以自己生成，单位秒，得是10位数。这里用的是后台传的参数
	// 注意要点： 五个参数要按开头字母的字母表顺序进行排列拼接。并采用驼峰式命名。（后台去写的话，也得用驼峰式命名写法，不能是下划线的写法，比如nonce_str）
	// 最后进行md5加密，并全部转成大写。填写到 方法传参的 paySign 属性值中。
	// let timeStamp = parseInt(new Date().getTime() / 1000)
	let signStr =
		`appId=${data.appid}&nonceStr=${data.nonce_str}&package=${data.package}&signType=MD5&timeStamp=${data.timeStamp}&key=58Lei2Yan95kE42jI17mo87TI5312640`
	let sign = md5(signStr).toUpperCase()
	// 以上代码是生成二次签名用
	return new Promise((resolve, reject) => {
		let obj = {
			appId: data.appid, //公众号名称，由商户传入         
			timeStamp: data.timeStamp,
			nonceStr: data.nonce_str, //随机串     
			package: data.package,
			signType: "MD5", //微信签名方式：     
			paySign: sign //微信签名 
		}
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest', obj,
			function(res) {
				if (res.err_msg == "get_brand_wcpay_request:ok") {
					// 使用以上方式判断前端返回,微信团队郑重提示：
					//res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
					resolve({
						state: 1,
						msg: res
					})
				} else if (res.err_msg == "get_brand_wcpay_request:cancel") {
					// 手动取消该订单支付
					resolve({
						state: 0,
						msg: res
					})
				} else {
					// 订单支付失败
					resolve({
						state: -1,
						msg: res
					})
				}
			});
	})
}


export const mstore = {
	setItem(name = 'name', item = {}) {
		uni.setStorageSync(name, item)
	},
	getItem(name = 'name') {
		return uni.getStorageSync(name)
	},
	removeItem(name = 'name') {
		uni.removeStorageSync(name)
	}
}

export function getOrderStatus(isRefund, orderYn, orderStatus, distributionId, paramTime) {
	console.log(isRefund, orderYn, orderStatus, distributionId, paramTime);
	// 有order_status  10:待付款 20:待配送 25:部分发货 30:待收货 99:完成  
	// 也有：is_refund 是否退货（1：否，2：退货审核，3：退货完成）  也有order_yn：取消订单(1:否，2:是)'
	const obj = {
		status: '',
		text: ''
	}
	const _isRefund = parseInt(isRefund)
	const _orderYn = parseInt(orderYn)
	const _orderStatus = parseInt(orderStatus)
	const _distributionId = parseInt(distributionId)
	const _paramTime = paramTime


	const strArr = _paramTime && _paramTime.replace(/-|:/g, ' ').split(' ')
	let date = _paramTime ? new Date(strArr[0], strArr[1] - 1, strArr[2], strArr[3], strArr[4], strArr[5]).getTime() : +
		new Date()
	let timeStart = new Date().setHours(0, 0, 0, 0)
	let time9_30 = timeStart + 9.5 * 60 * 60 * 1000
	let time17_30 = timeStart + 17.5 * 60 * 60 * 1000

	let sendTypeText = ''

	if (_distributionId == 1) {
		const time = formatDate(date, 'HH:mm')
		const hourHalf = formatDate(date + 60 * 60 * 1000, 'HH:mm')
		if (date > time9_30 && date < time17_30) {
			sendTypeText = `${hourHalf}`
		} else {
			sendTypeText = '明日'
		}
	} else if (_distributionId == 2) {
		// const day = parseInt(formatDate(date, ''))
		sendTypeText = '明日送达'
	} else {
		const strArr = _paramTime && _paramTime.replace(/-|:/g, ' ').split(' ')
		let timeStamp = _paramTime ? new Date(strArr[0], strArr[1] - 1, strArr[2], strArr[3], strArr[4], strArr[5]).getTime() :
			+new Date()
		const after3 = timeStamp + 3 * 24 * 60 * 60 * 1000
		const dayAfter3 = formatDate(after3, 'MM月DD日')
		sendTypeText = `${dayAfter3}`
	}




	if (_isRefund === 3 || _isRefund === 2) {
		obj.status = _isRefund === 3 ? '已退货' : _isRefund === 2 ? '退货审核中' : '已完成'
		obj.text = _isRefund === 3 ? '' : _isRefund === 2 ? '' : ''
		obj.code = _isRefund

		return obj
	}

	if (_orderYn === 2) {
		obj.status = '已取消'
		obj.code = _orderYn
		return obj
	}

	obj.code = _orderStatus
	if (_distributionId == 3) {
		switch (_orderStatus) {
			case 10:
				obj.status = '待付款'
				break
			case 20:
				obj.status = '待发货'
				obj.text = `支付成功，订单即将发货。预计${sendTypeText}送达，请耐心等待`
				break
			case 25:
				obj.status = '部分发货'
				obj.text = `支付成功，订单即将发货。预计${sendTypeText}送达，请耐心等待`
				break
			case 30:
				obj.status = '待收货'
				obj.text = `商品正在发货中，预计${sendTypeText}送达，请耐心等待。`
				break
			case 99:
				obj.status = '已完成'
				obj.text = '订单已完成，感谢您的支持，MOTI到家平台的所有商品均为官方正品。'
				break
		}
	} else if (_distributionId == 2) {
		switch (_orderStatus) {
			case 10:
				obj.status = '待付款'
				break
			case 20:
				obj.status = '待发货'
				obj.text = '支付成功，订单即将发货。预计次日送达，请耐心等待'
				break
			case 25:
				obj.status = '待收货'
				obj.text = '商品正在配送，预计次日送达，请耐心等待'
				break
			case 30:
				obj.status = '待收货'
				obj.text = '商品正在配送，预计次日送达，请耐心等待'
				break
			case 99:
				obj.status = '订单已送达'
				obj.text = '订单已完成，感谢您的支持，MOTI到家平台的所有商品均为官方正品，您可以放心使用。'
				break
		}
	} else {
		switch (_orderStatus) {
			// 3-已被抢单 5-商家已商品校验 7-订单已推送到蜂鸟系统 9-系统拒单/配送异常 11-蜂鸟系统已接单 13-已分配骑手 15-骑手已到店 17-配送中 19-已送达 21-商品签收完成 23-抢单已取消 25-商品被拒签 27-商品申请退货中 29-商品退货退款完成
			case 10:
				obj.status = '待付款'
				break
			case 3:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 5:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 7:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 9:
				obj.status = '系统拒单/配送异常'
				obj.text = '系统拒单/配送异常'
				break
			case 11:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 13:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 15:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 17:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 19:
				obj.status = '订单已送达'
				obj.text = '订单已送达。感谢您的支持，MOTI到家平台的所有商品均为官方正品。'
				break
			case 21:
				obj.status = '订单已完成'
				obj.text = '订单已送达，感谢您的支持，MOTI到家平台的所有商品均为官方正品，您可以放心使用。'
				break
			case 23:
				obj.status = '抢单已取消'
				obj.text = '抢单已取消'
				break
			case 25:
				obj.status = '商品被拒签'
				obj.text = '商品被拒签'
				break
			case 27:
				obj.status = '商品申请退货中'
				obj.text = '商品申请退货中'
				break
			case 29:
				obj.status = '商品退货退款完成'
				obj.text = '商品退货退款完成'
				break
		}
	}

	return obj
}


export const device = () => {
	const ua = window.navigator.userAgent;
	const isAndroid = /(Android);?[\s/]+([\d.]+)?/.test(ua);
	const isIpad = /(iPad).*OS\s([\d_]+)/.test(ua);
	const isIpod = /(iPod)(.*OS\s([\d_]+))?/.test(ua);
	const isIphone = !isIpad && /(iPhone\sOS)\s([\d_]+)/.test(ua);
	const isWindowsWechat = /WindowsWechat/i.test(ua);
	const isWechat = !isWindowsWechat && /micromessenger/i.test(ua);
	const isWeibo = /WeiBo/i.test(ua);
	const isQQ = /QQ\//.test(ua);
	return {
		isAndroid,
		isIpad,
		isIpod,
		isIphone,
		isWindowsWechat,
		isWechat,
		isWeibo,
		isQQ
	}
}

export const navTo = (url) => {
	if (!isLogin()) {
		uni.navigateTo({
			url: '/pages/login/login'
		})
	} else {
		uni.navigateTo({
			url: url
		})
	}
}
	
	
	
export default {
	isLogin,
	checkLogin,
	getCookie,
	cartCache,
	mstore,
	navTo,
	flatten(arr) {
		let res = []
		for (var i = 0; i < arr.length; i++) {
			if (Array.isArray(arr[i])) {
				res = res.concat(flatten(arr[i]));
			} else {
				res.push(arr[i]);
			}
		}
		return res
	},
	getCityInfo(addressStr) {
		return new Promise((resolve, reject) => {
			let cityCode = ''
			let address = ''
			const flaArr = this.flatten(cityData)
			for (let i = 0; i < flaArr.length; i++) {
				if (addressStr.indexOf(flaArr[i].label) > -1) {
					cityCode = flaArr[i].value
					address = flaArr[i].label
					break
				}
			}
			resolve({
				cityCode,
				address
			})
		})
	}
}
