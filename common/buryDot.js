const reports = {
	index: (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			city: _this.address, // 城市（省市级）
			hour_arrival: _this.isOpenedOneHour, // 是否为一小时达城市
		}
	},
	// 我的
	'page_mine': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）
			phone: userInfo.mobile,
			register_status: !!_this.$isLogin()
		}
	},
	// 购物车
	'page_cart': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		const spuNames = []
		const skuNames = []
		const prices = []
		const nums = []
		for (let item of _this.hourItems) {
			spuNames.push(item.spuName)
			skuNames.push(item.skuName)
			prices.push(item.price)
			nums.push(item.num)
		}
		for (let item of _this.averageItems) {
			spuNames.push(item.spuName)
			skuNames.push(item.skuName)
			prices.push(item.price)
			nums.push(item.num)
		}
		return {
			city: _this.$store.getters.city, //城市（省市级）
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			spu_name: spuNames, // 商品SPU名称
			sku_name: skuNames, // 商品SKU名称
			quantity: prices, // 商品数量
			price: nums, // 商品单价"
		}
	},
	// 商品详情页
	'page_good_detail': (_this) => {
		// 每一次获取sku就发送一次
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, //城市（省市级）
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			hour_arrival: _this.sendType === 'hour', // 是否为一小时达
			spu_name: _this.goods.name, // 商品SPU名称
			sku_name: _this.choosedAttr, // 商品SKU名称
			quantity: _this.changedNumber, // 商品数量
			price: _this.goods.market_price, // 商品单价"
		}
	},
	// 提交订单
	'page_order_info': (_this) => {
		// tips: 这个是拿当前登陆者登录的手机号还是当前收货地址的手机号?
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.address.receivePhone, // 手机号
		}
	},
	// 配送方式选择
	'page_send_way': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
		}
	},
	// 选择优惠券
	'page_discounts': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
		}
	},
	// 支付订单
	'page_pay_way': (_this) => {
		console.log(_this.orderInfo);
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.orderInfo.address.cityName, //城市（省市级） tips: 是收货地址的城市还是当前所在城市
			phone: _this.orderInfo.address.receivePhone, // 手机号
			spu_name: _this.orderInfo.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.orderInfo.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.orderInfo.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.orderInfo.orderNo, //订单号
			ord_amt: _this.orderInfo.orderMoney, // 订单总额
			ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			choo_coup_amount: _this.orderInfo.orderDiscountMoney || 0, //使用优惠券金额
			buyer_name: _this.orderInfo.address.receiveName, // 姓名
			address: _this.orderInfo.address.districtName + _this.orderInfo.address.userAddress, //详细地址
			delivery_way: _this.orderInfo.distributionId, // 配送方式"
		}
	},
	// 我的订单
	'page_my_order': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			ord_quantity: _this.orderList.myOrder.length, //订单数量
			phone: userInfo.mobile
		}
	},
	// 订单支付成功
	'page_payway_success': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, //城市（省市级） tips: 是收货地址的城市还是当前所在城市
			phone: _this.orderInfo.address.receivePhone, // 手机号
			spu_name: _this.orderInfo.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.orderInfo.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.orderInfo.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.orderInfo.orderNo, //订单号
			ord_amt: _this.orderInfo.orderMoney, // 订单总额
			ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			choo_coup_amount: _this.orderInfo.orderDiscountMoney || 0, //使用优惠券金额
			buyer_name: _this.orderInfo.address.receiveName, // 姓名
			address: _this.orderInfo.address.districtName + _this.orderInfo.address.userAddress, //详细地址
			delivery_way: _this.orderInfo.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式"
		}
	},
	// 订单详情
	'page_ord_detail': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			// 商品信息
			spu_name: _this.orderInfo.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.tradeOrderItemVos[0].skuName, // SKU名称
			quantity: _this.orderInfo.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.orderInfo.tradeOrderItemVos[0].skuPrice, // 商品单价
			// 订单信息
			ord_status: _this.orderInfo.orderStatus, // 订单状态
			ord_num: _this.orderInfo.orderNo, // 订单号
			ord_amt: _this.orderInfo.orderMoney, // 订单总额
			ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			// 收货信息
			buyer_name: _this.orderInfo.address.receiveName, // 姓名
			phone: _this.orderInfo.address.receivePhone, // 手机号
			address: _this.orderInfo.address.districtName + _this.orderInfo.address.userAddress, // 详细地址

			// 配送信息
			delivery_way: _this.orderInfo.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式
		}
	},
	// 收货地址
	'page_address': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
		}
	},
	// 我的优惠券（与选择优惠券一致）
	'page_my_discounts': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
			coup_quantity: _this.couponList.length, // 优惠券数量"
		}
	},
	// 兑换优惠券
	'page_exchange_counts': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
		}
	},
	// 实名认证
	'page_name_auth': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.authInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			city: _this.$store.getters.city, // 城市（省市级）
			buyer_name: _this.authInfo.card_name, //姓名
			id_card_num: _this.authInfo.card_id, //身份证号
			auth_status: _this.authStatus == 1 ? '已认证' : '未认证', // 认证状态
		}
	},
	// 实名认证成功
	'page_auth_success': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.phoneNumber, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			city: _this.$store.getters.city, // 城市（省市级）
			// 18_yes_no: !_this.isUn18ed, // 是否未满18岁
		}
	},
	// 实名认证失败
	'page_auth_fail': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.phoneNumber, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			city: _this.$store.getters.city, // 城市（省市级）
		}
	},


	/**
	 * 按钮埋点
	 */

	// 登录按钮
	'login_login': (_this) => {
		// tips: 分为获取手机号登录和手机号验证码登录
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: _this.userInfo.mobile, // 手机号
			register_status: _this.isRegist ? '新用户' : '老用户', // 注册状态 tips: 这是个什么状态
			hour_arrival: _this.$store.getters.isOneHour, // 是否为一小时达城市
		}
	},
	// 立即领券
	'index_imme_get_coup': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			get_status: _this.gotNewUserCouponsSatus, // 领取状态
		}
	},
	// 广告位4张轮播图
	'index_pic_adv': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			picture_id: _this.banners[_this.currentBannerIndex].id, // 轮播图ID
		}
	},
	// 商品列表
	'index_good_list': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), // 注册状态tips:这个位置恐怕是拿不到注册状态, 应该是登录状态吧
		}
	},
	// 登陆领取: 首页弹窗 5折新人券弹窗
	'index_coup_sign_get': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
		}
	},
	// 立即报名:一小时达未开城
	'index_imme_appl': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(),
		}
	},
	// 去结算（普通快递）
	'cart_aver_close_acc': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		const spuNames = []
		const skuNames = []
		const quantitys = []
		const prices = []
		_this.averageItems.froEach((cur) => {
			spuNames.push(cur.spuName)
			skuNames.push(cur.skuName)
			quantitys.push(cur.num)
			prices.push(cur.price)
		})
		return {
			phone: userInfo.mobile, // 手机号  
			spu_name: spuNames, // 商品SPU名称
			sku_name: skuNames, // SKU名称
			quantity: quantitys, // 商品数量
			price: quantitys, //  商品单价
			total_amount: _this.averageTotalSum, // 商品总金额
			settlement_kind: 'average', // 结算分类
		}
	},
	// 去结算（一小时达）
	'cart_hour_close_acc': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		const spuNames = []
		const skuNames = []
		const quantitys = []
		_this.hourItems.froEach((cur) => {
			spuNames.push(cur.spuName)
			skuNames.push(cur.skuName)
			quantitys.push(cur.skuNum)
			prices.push(cur.price)
		})
		return {
			phone: userInfo.mobile, // 手机号
			spu_name: spuNames, // 商品SPU名称
			sku_name: skuNames, // SKU名称
			quantity: quantitys, // 商品数量
			price: prices, //  商品单价
			total_amount: _this.hourTotalSum, // 商品总金额
			settlement_kind: 'hour', // 结算分类
		}
	},
	// 我的优惠券
	'mine_my_disc_coup': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号  
			// coup_status:'tips:似乎是没有状态', // 优惠券状态
			amount: _this.coupon.denomination ? _this.coupon.denomination : `${_this.coupon.discountRate * 10}折`, // 金额 tips: 有折扣券, 有满减券, 到底怎么显示
			get_time: _this.coupon.createTime, // 领取时间
			expire_time: _this.coupon.endTime, //  过期时间
			// use_condition:'tips:似乎没有这个值啊', // 使用条件
		}
	},
	// 我的订单
	'mine_my_ords': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号  
			ord_quantity: _this.orderList.myOrder.length, // 订单数量 tips:点击的时候是获取不到的, 进了页面才能获取到
		}
	},
	// 联系客服
	'my_order_cust_server': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号  
		}
	},
	// 取消订单确定
	'ord_detail_confirm': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		console.log(_this);
		return {
			city: _this.$store.getters.city, //城市（省市级）tips:这里是当前这个人所在城市还是订单的收货地址城市
			// phone: _this.options.address.receivePhone, // 手机号
			phone: userInfo.mobile,
			spu_name: _this.options.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.options.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.options.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.options.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.options.orderNo, //订单号
			ord_amt: _this.options.orderMoney, // 订单总额
			ord_create_time: _this.options.createTime, // 订单创建时间
			choo_coup_amount: _this.options.orderDiscountMoney, //使用优惠券金额
			buyer_name: _this.options.receiveName, // 姓名
			// address: _this.options.districtName + _this.options.userAddress, //详细地址
			// tips: 需要列表里面加上address
			delivery_way: _this.options.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式"
		}
	},
	// 订单列表:去支付
	'ord_detail_pay_ord': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			// tips: 需要接口加上address
			city: _this.options.address.cityName, //城市（省市级）tips:这里是当前这个人所在城市还是订单的收货地址城市
			phone: _this.options.address.receivePhone, // 手机号
			spu_name: _this.options.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.options.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.options.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.options.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.options.orderNo, //订单号
			ord_amt: _this.options.orderMoney, // 订单总额
			ord_create_time: _this.options.createTime, // 订单创建时间
			choo_coup_amount: _this.options.orderDiscountMoney, //使用优惠券金额
			buyer_name: _this.options.receiveName, // 姓名
			address: _this.options.districtName + _this.options.userAddress, //详细地址
			delivery_way: _this.options.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式"
		}
	},
	// 兑换优惠券确定
	'exchange_counts_confirm': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号  
			exchang_code: _this.couponCode, // 兑换码
			exchange_status: _this.exchangeRes.info, // 兑换状态
			// amount: '',   // 金额 tips: 没有金额
			get_time: _this.exchangeRes.pull_time, // 领取时间
			// expire_time:'', //  过期时间 tips: 没有过期时间
			// use_condition:'', // 使用条件 tips: 没有使用条件
			// "{
			// 	"pull_time":"2019-09-19 14:33:35",
			// 	"coupon_code":"SS0112",
			// 	"user_id":"618810440352485376",
			// 	"showFlag":true,
			// 	"info":"兑换成功！"
			// }"
		}
	},
	// 新建收货地址保存
	'newaddress_save_addr': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.addressInfo.userPhone, // 手机号  
			buyer_name: _this.addressInfo.userName, // 姓名
			'province-city-district': _this.addressInfo.address, // 省市区
			addr: _this.addressInfo.addressDetail, // 详细地址
		}
	},
	// 编辑收货地址保存
	'editaddress_save_addr': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.addressInfo.userPhone, // 手机号
			buyer_name: _this.addressInfo.userName, // 姓名
			province_city_district: _this.addressInfo.address, // 省市区
			addr: _this.addressInfo.addressDetail, // 详细地址
		}
	},
	// 删除收货地址
	'editaddress_del_addr': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: _this.addressInfo.userPhone, // 手机号
			buyer_name: _this.addressInfo.userName, // 姓名
			province_city_district: _this.addressInfo.address, // 省市区
			addr: _this.addressInfo.addressDetail, // 详细地址
		}
	},
	// 确定按钮: 加入购物车
	'good_detail_add_shopcart_conf': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号  
			spu_name: _this.goods.spuName, // 商品SPU名称
			sku_name: _this.choosedAttr, // SKU名称
			quantity: _this.changedNumber, // 商品数量
			price: _this.goods.market_price, //  商品单价
		}
	},
	// 确定按钮: 购买
	'good_detail_imme_buy_conf': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
			spu_name: _this.goods.spuName, // 商品SPU名称
			sku_name: _this.choosedAttr, // SKU名称
			quantity: _this.changedNumber, // 商品数量
			price: _this.goods.market_price, //  商品单价
		}
	},
	// 弹窗: 加入购物车
	'good_detail_add_shopcart_choo': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
			spu_name: _this.goods.spuName, // 商品SPU名称
			sku_name: _this.choosedAttr, // SKU名称
			quantity: _this.changedNumber, // 商品数量
			price: _this.goods.market_price, //  商品单价
		}
	},
	// 弹窗: 立即购买
	'good_detail_imme_buy_choo': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			phone: userInfo.mobile, // 手机号
			spu_name: _this.goods.spuName, // 商品SPU名称
			sku_name: _this.choosedAttr, // SKU名称
			quantity: _this.changedNumber, // 商品数量
			price: _this.goods.market_price, //  商品单价
		}
	},
	// 确认支付
	'order_info_submit_pay': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			// tips: 是预订单的信息还是生成了订单后的信息
			city: _this.$store.getters.city, //城市（省市级） tips: 是收货地址的城市还是当前所在城市
			phone: _this.address.receivePhone, // 手机号
			spu_name: _this.orderInfo.itemList[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.itemList[0].skuName, // 商品SKU名称
			quantity: _this.orderInfo.itemList[0].skuNum, // 商品数量
			price: _this.orderInfo.itemList[0].skuPrice, // 商品单价
			// ord_num: _this.orderNo, //订单号
			ord_amt: _this.orderInfo.orderVo.orderMoney, // 订单总额
			// ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			choo_coup_amount: _this.orderInfo.orderDiscountMoney || 0, //使用优惠券金额
			buyer_name: _this.address.receiveName, // 姓名
			address: _this.address.districtName + _this.address.userAddress, //详细地址
			// delivery_way: _this.orderInfo.distributionId, // 配送方式"
		}
	},
	// 订单待支付状态: 去支付
	'pay_way_pay_ord': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.orderInfo.address.cityName, //城市（省市级） tips: 是收货地址的城市还是当前所在城市
			phone: _this.orderInfo.address.receivePhone, // 手机号
			spu_name: _this.orderInfo.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.orderInfo.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.orderInfo.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.orderInfo.orderNo, //订单号
			ord_amt: _this.orderInfo.orderMoney, // 订单总额
			ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			choo_coup_amount: _this.orderInfo.orderDiscountMoney || 0, //使用优惠券金额
			buyer_name: _this.orderInfo.address.receiveName, // 姓名
			address: _this.orderInfo.address.districtName + _this.orderInfo.address.userAddress, //详细地址
			delivery_way: _this.orderInfo.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式"
		}
	},
	// 确认收货
	'ord_detail_get_good_conf': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.orderInfo.address.cityName, //城市（省市级） tips: 是收货地址的城市还是当前所在城市
			phone: _this.orderInfo.address.receivePhone, // 手机号
			spu_name: _this.orderInfo.tradeOrderItemVos[0].spuName, // 商品SPU名称
			sku_name: _this.orderInfo.tradeOrderItemVos[0].skuName, // 商品SKU名称
			quantity: _this.orderInfo.tradeOrderItemVos[0].skuNum, // 商品数量
			price: _this.orderInfo.tradeOrderItemVos[0].skuPrice, // 商品单价
			ord_num: _this.orderInfo.orderNo, //订单号
			ord_amt: _this.orderInfo.orderMoney, // 订单总额
			ord_create_time: _this.orderInfo.createTime, // 订单创建时间
			choo_coup_amount: _this.orderInfo.orderDiscountMoney || 0, //使用优惠券金额
			buyer_name: _this.orderInfo.address.receiveName, // 姓名
			address: _this.orderInfo.address.districtName + _this.orderInfo.address.userAddress, //详细地址
			delivery_way: _this.orderInfo.distributionId == 1 ? '一小时达' : _this.options.distributionId == 2 ? '次日达' : '普通快递', // 配送方式"
		}
	},
	// 分享按钮
	'good_detail_share': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), // 登录状态
			hour_arrival: _this.$store.getters.isOneHour, // 是否为一小时达城市
		}
	},
	// 分享给好友
	'good_detail_share_friends': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile || '未登录', // 手机号
			register_status: !!_this.$isLogin(), // 登录状态
			spu_name: _this.goods.name,
			hour_arrival: _this.$store.getters.isOneHour, // 是否为一小时达城市
			price: _this.goods.market_price
		}
	},
	// 生成分享海报
	'good_detail_gene_poster': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), // 登录状态
			spu_name: _this.goods.name,
			hour_arrival: _this.$store.getters.isOneHour, // 是否为一小时达城市
			price: _this.goods.market_price
		}
	},
	// 生成分享海报, 保存
	'gene_poster_save_poster': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: userInfo.mobile, // 手机号
			register_status: !!_this.$isLogin(), // 登录状态
			spu_name: _this.goods.name,
			hour_arrival: _this.$store.getters.isOneHour, // 是否为一小时达城市
			price: _this.goods.market_price
		}
	},
	// 提交认证
	'name_auth_submit_auth': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: _this.phoneNumber, // 手机号
			buyer_name: _this.realFullName || _this.realName, //姓名
			// 18_yes_no: !_this.isUn18ed, // 是否满18岁
			auth_status: _this.isAuthed ? '已认证' : '未认证', // 认证状态
		}
	},
	// 认证成功后购买
	'name_auth_imme_buy': (_this) => {
		const userInfo = uni.getStorageSync('userInfo')
		return {
			city: _this.$store.getters.city, // 城市（省市级）  
			phone: _this.phoneNumber, // 手机号
			register_status: !!_this.$isLogin(), //登录状态
			auth_status: _this.isAuthed ? '已认证' : '未认证', //认证状态
			spu_name: _this.tapedGoods.name, // 商品SPU名称
			price: _this.tapedGoods.market_price, // 商品单价
		}
	},
}
export default function wxBuryDot(name) {
	// 只有小程序才真正执行代码
	// #ifdef MP-WEIXIN
	// console.log('wxBuryDot', name, reports[name](this));
	const obj = reports[name](this)
	// 埋点所有的类型都是字符串
	for (let item in obj) {
		obj[item] = String(obj[item])
	}
	console.log('reportAnalytics', obj);
	wx.reportAnalytics(name, obj)
	// #endif
}
