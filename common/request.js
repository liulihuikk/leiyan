import HTTP from './HTTP.js'
import baseConfig from './config.js'
// #ifdef MP-WEIXIN
import store from '@/store/index.js'
// #endif

/*
	get请求
*/
const get = async (api, data, header = {}, config = {}) => {
	// #ifdef MP-WEIXIN
	const token = uni.getStorageSync('mt_token')
	header = {
		token: token,
		'content-type': 'application/x-www-form-urlencoded'
	}
	// #endif
	const res = await HTTP.get(api, data, header, config)
	if (res.code === '0') {
		if (!isNaN(parseInt(res.result))) {
			res.result = res.result
		} else {
			res.result = typeof res.result === 'string' ? JSON.parse(res.result) : res.result
		}
		return res
	} else {
		return res
	}
}



/*
	post请求
*/
const post = async (api, data, header = {}, config = {}) => {
	// #ifdef MP-WEIXIN
	const token = uni.getStorageSync('mt_token')
	header = {
		token: token,
		'content-type': 'application/x-www-form-urlencoded'
	}
	// #endif
	const res = await HTTP.post(api, data, header, config)
	if (!res.result) res.result = []
	if (res.code === '0') {
		res.code = Number(res.code)
		try {
			if (!isNaN(parseInt(res.result))) {
				res.result = res.result
			} else {
				res.result = typeof res.result === 'string' ? JSON.parse(res.result) : res.result
			}
			return res
		} catch (e) {
			return res
			//TODO handle the exception
		}
	} else {
		return res
	}
}
/*
    json格式post请求
*/
const postJSON = async (api, data, header = {}, config = {}) => {
	// #ifdef MP-WEIXIN
	const token = uni.getStorageSync('mt_token')
	header = {
		token: token,
		// 'content-type': 'application/x-www-form-urlencoded'
	}
	// #endif
	const res = await HTTP.postJSON(api, data, header, config)
	// console.log(`%c ${api}`, 'background-color:#0f0;', res)
	if (!res.result) res.result = []
	if (res.code === '0') {
		res.code = Number(res.code)
		try {
			if (!isNaN(parseInt(res.result))) {
				res.result = res.result
			} else {
				res.result = typeof res.result === 'string' ? JSON.parse(res.result) : res.result
			}
			return res
		} catch (e) {
			return res
			//TODO handle the exception
		}

	} else if (res.code === '3' && res.msg === '未登录') {
		// const url = location.href
		// const isIndexGetCar = (/\/index\/index/.test(url) || !/pages/.test(url)) && /getCar/.test(api)
		// if (!isIndexGetCar) {
		// 	uni.navigateTo({
		// 		url: '/pages/login/login'
		// 	})
		// }
		return res
	} else {
		// uni.showToast({
		// 	title: `${res.msg}`,
		// 	icon: 'none'
		// })

		return res
	}
}

/*
	form格式post请求
*/
const postForm = async (api, data, header = {}, config = {}) => {
	// #ifdef MP-WEIXIN || APP-PLUS
	const token = uni.getStorageSync('mt_token')
	header = {
		token: token,
		'content-type': 'application/x-www-form-urlencoded'
	}
	// #endif
	const res = await HTTP.postForm(api, data, header, config)
	console.log(`${api}`, res)
	if (!res.result) res.result = []
	if (res.code === '0') {
		res.code = Number(res.code)
		try {
			if (!isNaN(parseInt(res.result))) {
				res.result = res.result
			} else {
				res.result = typeof res.result === 'string' ? JSON.parse(res.result) : res.result
			}
			return res
		} catch (e) {
			return res
			//TODO handle the exception
		}
	} else if (res.code === '3' && /未登录/.test(res.msg)) {
		// #ifdef MP-WEIXIN
		// uni.clearStorageSync()
		// uni.redirectTo({
		// 	url: '/pages/login/login'
		// })
		// #endif
		// const url = location.href
		//       const isIndexGetCar = (/\/index\/index/.test(url) || !/pages/.test(url)) && /getCar/.test(api)
		//       if (!isIndexGetCar) {
		// 	uni.navigateTo({
		// 		url: '/pages/login/login'
		// 	})
		//       }
		return res
	} else {
		// uni.showToast({
		// 	title: `${res.msg}`,
		// 	icon: 'none'
		// })
		return res
	}
}


/**
 * 上传文件
 */

const upload = async (api, file, fileName, formData = {}, header = {}, config = {}) => {
	// #ifdef MP-WEIXIN || APP-PLUS
	const token = uni.getStorageSync('mt_token')
	header = {
		token: token,
		// 'content-type': 'application/x-www-form-urlencoded'
	}
	// #endif
	const res = await HTTP.upload(api, file, fileName, formData, header, config)
	
	return res
}



/**
 * 首页投票
 */
export const vote = async (areaId, userId) => {
	return await postJSON('/mall/h5/voteCity/vote', {
		areaId,
		userId
	})
}
// 查询所有区域投票数
export const queryVoteNums = async (areaId = 0) => {
	return await postJSON(`/mall/h5/voteCity/vote/${areaId}`)
}
// 查询用户是否客户以投票
export const queryCanVote = async (areaId, userId) => {
	return await postJSON(`/mall/h5/voteCity/vote/${areaId}/${userId}`)
}
// 查询当前城市是否开通一小时达

export const queryHourCity = async (areaId) => {
	return await postJSON(`/mall/h5/voteCity/onehour/city/${areaId}`)
}

/**
 * 打卡
 */
// 查询用户打开记录
export const queryPointDataByUserId = async () => {
	return await postForm('/mall/h5/punchCard/queryPointDataByUserId')
}
//打卡
export const punchCard = () => {
	return postForm('/mall/h5/punchCard/punchCard', {

	})
}
// export const saveCouponCode = async (couponId) => {
//  	return post('/mall/h5/couponInfo/saveCouponCode', {
// 		couponId
//  	})
// }
// 订单详情查询销售量
export const countGoodsSellerEachMonth = async (id) => {
	return await postForm('/mall/h5/goodsSpu/countGoodsSellerEachMonth', {
		spuId: id
	})
}






/*
 * 商品相关接口
 * =================================================================================================================================================
 */

// 请求首页轮播图
export const rotationInfoList = async () => {
	return await postForm('/mall/h5/goodsSpu/rotationInfoList')
}

// 主页tab选项
export const queryCategroyList = async (parentId = -1) => {
	return await postForm('/mall/h5/goodsSpu/queryCategroyList', {
		parentId
	})
}

// 根据品类获取商品列表
export const queryGoodsSpuByCategroy = async (cId, page = 1, rows = 10) => {
	return await postForm('/mall/h5/goodsSpu/queryGoodsSpuByCategroy', {
		cId,
		page,
		rows
	})
}

// 根据id获取商品详情
export const goodsSpuQuery = async (spuId) => {
	return await postForm('/mall/h5/goodsSpu/goodsSpuQuery', {
		id: spuId
	})
}
// 查询sku数据查询接口局, spuId, attributes商品属性id
export const queryGoodsSku = async (spuId, attributes) => {
	return await postForm('/mall/h5/goodsSku/queryGoodsSku', {
		spuId,
		attributes
	})
}

// 查询一小时达商品
export const queryHourSpu = async (page = 1, rows = 10) => {
	return await postForm('/mall/h5/goodsSpu/queryHourSpu', {
		page,
		rows
	})
}
// 查询一小时达商品
export const goodsHourSpuQuery = async (id) => {
	return await postForm('/mall/h5/goodsSpu/goodsHourSpuQuery', {
		id
	})
}










/*
 * 用户相关
 * ======================================================================================================================
 */
export const authWechat = async (code) => {
	return await postForm('/mall/h5/pay/confirmAuthorization', {
		code,
	})
}
export const sendDynamicCode = async (mobile, codeType) => {
	// 101注册 102登录 103修改密码 104绑定手机
	return await postForm('/mall/h5/code/sendDynamicCode', {
		mobile,
		codeType
	})
}
export const regist = async (mobile, dynamicCode, loginName, password) => {
	return await postForm('/mall/h5/user/regist', {
		mobile,
		dynamicCode,
		loginName,
		password
	})
}
export const mobileLogin = async (data) => {
	// #ifdef H5
	return await postForm('/mall/h5/login/mobileLogin', data)
	// #endif
	// #ifdef MP-WEIXIN || APP-PLUS
	return await postForm('/mall/h5/login/appletMobileLogin', data)
	// #endif
}
// 小程序和app手机号验证码登录
export const appletMobileLogin = async (mobile, code, quickType) => {
	return await postForm('/mall/h5/login/appletMobileLogin', {
		mobile,
		dynamicCode: code,
		quickType
	})
}
// 账号密码登录
export const nameLogin = async (userName, pwd) => {
	return await postForm('/mall/h5/login/nameLogin', {
		loginName: userName,
		password: pwd
	})
}
export const loadInfo = async () => {
	const res = await postForm('/mall/h5/user/loadInfo')
	uni.setStorageSync('userInfo', res.result)
	return res.result.data
}
export const checkUserMobile = async (mobile) => {
	return await postForm('/mall/h5/user/checkUserMobile', {
		mobile
	})
}

// 微信小程序授权登录, 用code换取token
export const getToken = async (code) => {
	return await postForm(`/mall/h5/login/wxAppletLoginAuth`, {
		code
	})
}
// 微信小程序绑定手机号
export const wxAppletMobileLogin = async (mobile, userWxInfoCacheKey) => {
	return await postForm('/mall/h5/login/wxAppletMobileLogin', {
		mobile,
		userWxInfoCacheKey
	})
}

// 微信小程序：获取用户绑定的微信手机号接口, 解密过程
export const wxAppletGetBindMobileNumber = async (encryptedData, iv, userWxInfoCacheKey) => {
	return await postForm('/mall/h5/login/wxAppletGetBindMobileNumber', {
		encryptedData,
		iv,
		userWxInfoCacheKey
	})
}
// 登出
export const logout = async () => {
	return await postForm('/mall/h5/login/logout')
}





/**
 * 优惠券相关=============================================================================
 */

// 查询优惠券接口
export const showUserCouponList = async () => {
	return await postForm('/mall/h5/couponInfo/showUserCouponList')
}

// 领取优惠券接口
export const saveCouponCode = async (couponId) => {
	return await postForm('/mall/h5/couponInfo/saveCouponCode', {
		couponId
	})
}

// 兑换优惠券
export const redemptionCode = async (couponCode) => {
	return await postForm('/mall/h5/couponInfo/redemptionCode', {
		couponCode
	})
}

// 创建新用户优惠券
export const createNewCustomerCoupons = async (uid) => {
	return await postForm('/mall/h5/couponInfo/createNewCustomerCoupons', {
		userId: uid
	})
}

export const receiveNewCustomerCoupons = async (uid) => {
	return await postForm('/mall/h5/couponInfo/receiveNewCustomerCoupons', {
		userId: uid
	})
}

// 批量领取优惠券
export const batchDrawCoupons = async (couponIds, uid, times) => {
	return await postForm('/mall/h5/couponInfo/batchDrawCoupons', {
		p1: couponIds,
		p2: uid,
		p3: times
	})
}
















/*
 * 订单相关
 * ===========================================================================================================================================
 */


//通过买家Id返回退货单列表
export const getBuyerRefunds = async (buyerId, page, rows) => {
	return await postForm('/order/refund/getBuyerRefunds', {
		buyerId,
		page,
		rows
	})
}
// 获取配送方式
export function listDictByName(dictName) {
	return postForm('/mall/h5/user/dict/listDictByName', {
		dictName
	})

}


//退款
export const addRefund = async (refund = {}) => {
	let info = JSON.stringify(refund);
	return await postJSON('/order/refund/addRefund', {
		"refund": info
	})
}
export function getRefundItem(id) {
	return postForm('/order/refund/getRefundItem', {
		id
	})

}
//取消退款申请
export function updateRefundCancel(id) {
	return postForm('/order/refund/updateRefundCancel', {
		id
	})
}

// 下单生成订单接口
export const generateOrder = async (data = {}) => {
	return await postJSON('/order/m/generateOrder', data)
}
// 查询要结算商品预订单
export const queryPreOrderInfo = async (data) => {
	return await postJSON('/order/m/queryPreOrderInfo', data)
}

// 分页查询用户订单
export const listTradeOrder = async (page = 1, rows = 10) => {
	return await postForm('/order/m/listTradeOrder', {
		page,
		rows
	})
}
// 取消订单接口
export const cancelOrder = async (orderId) => {
	console.log('取消订单')
	return await postForm('/order/m/cancelOrder', {
		orderId
	})
}

// 支付前校验订单接口
export const checkOrder = async (data) => {
	return await postJSON('/order/m/checkOrder', data)
}
// 查询配送方式对应的配送费
export const getFreight = async (distributeId, money) => {
	return await postForm('/order/m/getFreight', {
		distributeId,
		money
	})
}
// 根据订单号查询物流信息
export const getOrderDeliveryInfo = async (orderId) => {
	return await postForm('/order/m/getOrderDeliveryInfo', {
		orderId
	})
}
// 根据订单id查询订单详情
export const tradeOrderDetail = async (id) => {
	return await postForm('/order/m/tradeOrderDetail', {
		id
	})
}

// 确认收货
export const confirmFinishOrder = async (orderId) => {
	return await postForm('/order/battleOrder/b/confirmFinishOrder', {
		orderId
	})
}

// 蓝牙预售下单前查询已购买量

export const checkBlueToothPreBookingNum = async () => {
	return await postForm('/order/m/checkBlueToothPreBookingNum')
}



/**
 * 支付方式
 */

export function aliWapPay(returnUrl, orderNo) {
	return postForm('/payment/alipay/wapPay', {
		returnUrl,
		orderNo
	})
}

// wxPay
export function wxPay(orderNo) {
	return postForm('/payment/wxpay/wapPay', {
		orderNo
	})
}

// 查询支付接口
export function queryPayStatus(orderNo, paymentType) { // 参数：  订单编号		支付类型：1——阿里	2——微信
	return postForm('/payment/pay/status', {
		orderNo,
		paymentType
	})
}

// 通过授权返回的code值来换取 网页授权access_token
export function getAccessToken(appId, secret, code) { // 参数： appId， 公众号的 appsecret， 以及 code值
	// 请求链接：
	let getAccessTokenUrl =
		`https://api.weixin.qq.com/sns/oauth2/access_token?appid=${appId}&secret=${secret}&code=${code}&grant_type=authorization_code`
	return get(getAccessTokenUrl)
}

export const wechatPay = async (orderNo, spuName) => {
	return await postForm('/mall/h5/pay/wxJsPay', {
		orderNo,
		spuName
	})
}

// 获取支付参数
export const getJsApiParam = async (prepayId) => {
	return await postForm('/mall/h5/pay/getJsApiParam', {
		prepayId
	})
}

// 获取prepay_id, 小程序支付
export const wxAppletPay = async (orderNo, spuName) => {
	return await postForm('/mall/h5/pay/wxAppletPay', {
		orderNo,
		spuName
	})
}

// 获取签名, 小程序支付
export const paySignAgain = async (prepayId) => {
	return await postForm('/mall/h5/pay/paySignAgain', {
		prepayId
	})
}






/*
 * 购物车相关接口
 * 
 * ============================================================================================================
 */
export const getCar = async () => {
	return await postForm('/mall/h5/car/getCar')
}

export const syncBuyCar = async (carSkuList) => { // 参数： 商品信息集合
	let items = []

	for (let i = 0; i < carSkuList.length; i++) {
		items.push({
			skuId: carSkuList[i].skuId,
			num: carSkuList[i].skuNum
		})
	}

	return await postForm('/mall/h5/car/syncBuyCar', {
		carSkuList: JSON.stringify(items)
	})
}
// 加入购物车
export const addCar = async (skuId, num) => {
	return await await postForm('/mall/h5/car/addCar', {
		skuId,
		num
	})
}

// 批量删除购物车 skuId: 多个用逗号分隔
export const delCar = async (skuIds) => {
	return await postForm('/mall/h5/car/subCar', {
		skuId: skuIds
	})
}
// 设置购物车数量
export const subCarNum = async (skuId, num) => {
	return await postForm('/mall/h5/car/subCarNum', {
		skuId,
		num
	})
}

export default {

}


/*================= 用户收货地址管理接口==================================*/

// 收货地址查询列表接口
export const listAddress = async (page, rows) => {
	// 101注册 102登录 103修改密码 104绑定手机
	return await postForm('/mall/h5/userAddress/listAddress', {
		page,
		rows
	})
}
// 新增收货地址接口
export const saveAddress = async (data, pickerValue) => {
	// 101注册 102登录 103修改密码 104绑定手机
	return await postForm('/mall/h5/userAddress/saveAddress', {
		receiveName: data.userName,
		receivePhone: data.userPhone,
		provinceCode: data.code_provinceCode,
		provinceName: data.provinceName,
		cityCode: data.code_cityCode,
		cityName: data.cityName,
		districtCode: data.code_districtCode,
		districtName: data.districtName,
		userAddress: data.addressDetail,
		pickerValue: pickerValue,
		lon: data.lon,
		lat: data.lat
	})
}

// 根据地址id查询收货地址接口
export const queryAddressById = async (id) => {
	// 101注册 102登录 103修改密码 104绑定手机
	return await postForm('/mall/h5/userAddress/queryAddressById', {
		id
	})
}
// 设置默认地址
export const defaultAddress = async (id) => {
	return await postForm('/mall/h5/userAddress/defaultAddress', {
		id
	})
}
// 删除收货地址
export const delAddress = async (id) => {
	return await postForm('/mall/h5/userAddress/deleteAddress', {
		id
	})
}
// 获取用户默认地址
export const queryUserDefaultAddress = async () => {
	return await postJSON('/mall/h5/userAddress/queryUserDefaultAddress')
}

// 收货地址修改接口
export const updateAddress = async (data, pickerValue) => {
	// 101注册 102登录 103修改密码 104绑定手机
	return await postForm('/mall/h5/userAddress/updateAddress', {
		isDefault: data.isDefault,
		receiveName: data.userName,
		receivePhone: data.userPhone,
		provinceCode: data.code_provinceCode,
		provinceName: data.provinceName,
		cityCode: data.code_cityCode,
		cityName: data.cityName,
		districtCode: data.code_districtCode,
		districtName: data.districtName,
		userAddress: data.addressDetail,
		pickerValue: pickerValue,
		id: data.id,
		lon: data.lon,
		lat: data.lat
	})
}




/**
 * 分享相关
 */

export const getShareParam = async (url) => {
	return await get(`/open/pub/wechat/jsapi/jssdk2`, {
		url: url
	})
}

//
export const activity = async (userId) => {
	return await postForm('/mall/h5/couponInfo/receiveNewCustomerCoupons', {
		userId
	})
}
export const activityExpress = async (orderId) => {
	return await get('/order/m/getOrderDeliveryInfoForSms', {
		orderId
	})
}


/**
 * =============================================================================================
 * 实名认证相关
 */
// 查询实名认证状态, 无入参
export const authenticateStatus = async () => {
	return await postForm('/mall/h5/user/authenticate/authenticateStatus')
}
// 上传用户反面身份证图像到阿里云
export const ocrScanBackIdCard = async (file) => {
	console.log('ocrScanFrontIdCard', file);
	return await upload('/mall/h5/user/authenticate/ocrScanBackIdCard', file, 'imageBack')
}
// 图像信息识别用户正面身份证，并上传身份证图像到阿里云
export const ocrScanFrontIdCard = async (file) => {
	console.log('ocrScanFrontIdCard', file);
	return await upload('/mall/h5/user/authenticate/ocrScanFrontIdCard', file, 'imageFront')
}
// 个人中心:查询用户的实名认证信息，无入参。后台获取userId当入参
export const queryAuthenticateInfo = async (orderId) => {
	return await postForm('/mall/h5/user/authenticate/queryAuthenticateInfo', {
		orderId
	})
}
// 个人中心:保存用户的实名认证申请信息
export const saveAuthenticateInfo = async (data) => {
	return await postForm('/mall/h5/user/authenticate/saveAuthenticateInfo', data)
}
// 个人中心:根据身份证id，姓名，手机号做三要素检查
export const threeFactorDetection = async (cardId, cardName, mobile) => {
	return await postForm('/mall/h5/user/authenticate/threeFactorDetection', {
		cardId,
		cardName,
		mobile
	})
}


/**
 * =========================================================
 * 获取去AccessToken, 小程序调用微信api的凭证
 * 
 */
export const wxAppletAccessToken = async () => {
	return await postForm('/mall/h5/login/wxAppletAccessToken')
}
// 小程序存储formId
export const saveFormIds = async (formId) => {
	// console.log('formIds', formIds);
	// const formIdsStr = formIds.join(',')
	// console.log('formIdsStr', formIdsStr);
	return await postForm('/mall/h5/login/saveFormIds', {
		formIds: formId
	})
}
export const querySpuConfigGoods = async () => {
	return await postForm('/mall/h5/goodsSpu/querySpuConfigGoods?systemUserType=1')
}

export const getWxaCodeUnLimit = async (page, scene) => {
	return await postForm('/mall/h5/wxshare/getWxaCodeUnLimit', {
		page,
		scene
	})
}
