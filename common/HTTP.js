import baseConfig from './config.js'

function isEmptyObj(obj = {}) {
	for (let i in obj) {
		return false
	}
	return true
}

export default {
	get(api, data, header = {}, config = {}) {
		const url = `${baseConfig.host}${api}`
		return new Promise((resolve, reject) => {
			uni.request({
				url,
				method: 'GET',
				data,
				header,
				...config,
				success(res) {
					resolve(res.data)
				},
				fail(err) {
					reject(err)
				}

			})
		})
	},
	post(api, data, header = {}, config = {}) {
		const url = `${baseConfig.host}${api}`
		return new Promise((resolve, reject) => {
			uni.request({
				url,
				method: 'POST',
				data,
				header,
				...config,
				success(res) {
					resolve(res.data)
				},
				fail(err) {
					reject(err)
				}

			})
		})
	},
	postJSON(api, data, header = {}, config = {}) {
		const url = `${baseConfig.host}${api}`
		// let url = ''
		// if (api.indexOf('mall/') > -1) {
		// 	url = `http://192.168.10.56:9091${api}`
		// } else if (api.indexOf('order/') > -1) {
		// 	url = `http://192.168.10.56:8098${api}`
		// } else {
		// 	url = `${baseConfig.host}${api}`
		// }
		return new Promise((resolve, reject) => {
			if (isEmptyObj(header)) {
				header = {
					'Content-Type': 'application/json;charset=utf-8'
				}
			}
			uni.request({
				url,
				method: 'POST',
				data,
				header,
				...config,
				success(res) {
					resolve(res.data)
				},
				fail(err) {
					reject(err)
				}

			})
		})
	},
	postForm(api, data, header = {}, config = {}) {
		// let url = ''
		// if (api.indexOf('mall/') > -1) {
		// 	url = `http://192.168.10.56:9091${api}`
		// } else if (api.indexOf('order/') > -1) {
		// 	url = `http://192.168.10.56:8098${api}`
		// } else {
		// 	url = `${baseConfig.host}${api}`
		// }
		const url = `${baseConfig.host}${api}`
		return new Promise((resolve, reject) => {
			if (isEmptyObj(header)) {
				header = {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}
			uni.request({
				url,
				method: 'POST',
				data,
				header,
				...config,
				success(res) {
					resolve(res.data)
				},
				fail(err) {
					reject(err)
				}

			})
		})
	},
	async upload(api, file, name, formData, ...rest) {
		const url = `${baseConfig.host}${api}`
		return new Promise((resolve, reject) => {
			let [header, config] = rest
			console.log('upload1', rest);
			if (isEmptyObj(header)) {
				// header = {
				// 	'Content-Type': 'application/json;charset=utf-8'
				// }
			}
			console.log('upload', file);
			uni.uploadFile({
				url: url,
				filePath: file,
				name: name,
				header: header,
				formData: formData,
				...config,
				success(res) {
					resolve(res.data)
				},
				fail(err) {
					reject(err)
				},
				complete(com) {
					console.log('complete', com);
				}
			})
		})
	},
}
