import cityData from '@/common/city-data/city.js'
export default {
	Amap: null,
	load(id = 'amp_container') {
		return new Promise((resolve, reject) => {
			this.Amap = new plus.maps.Map(id)
			console.log('Amap load:', this.Amap);
			resolve()
		})
	},
	getLocalCity() {
		return new Promise(async (resolve, reject) => {
			const _this = this
			if (!this.Amap) {
				await this.load()
			}
			this.Amap.getUserLocation(function(state, point) {
				if (0 == state) {
					_this.latAndLngToAddress(point.longitude, point.latitude).then((cityInfo) => {
						resolve(cityInfo.city)
					})
					// resolve(result.city)
				}
			});
		})
	},
	// 获取精确位置的IP地址
	getCurrentPosition() {
		return new Promise((resolve, reject) => {

		})
	},
	// 经纬度解析成地址
	latAndLngToAddress(lng, lat) {
		return new Promise((resolve, reject) => {
			const point = {
				longitude: lng,
				latitude: lat
			}
			// console.log(location);
			plus.maps.Map.reverseGeocode(point, {}, function(event) {
				console.log('reverseGeocode', event);
				// 北京市朝阳区崔各庄镇浦项中心A座绿地中心
				const addressStr = event.address
				const flatten = (arr) => {
					let res = []
					for (var i = 0; i < arr.length; i++) {
						if (Array.isArray(arr[i])) {
							res = res.concat(flatten(arr[i]));
						} else {
							res.push(arr[i]);
						}
					}
					return res
				}
				let cityCode = ''
				let city = ''
				const flaArr = flatten(cityData)
				for (let i = 0; i < flaArr.length; i++) {
					if (addressStr.indexOf(flaArr[i].label) > -1) {
						cityCode = flaArr[i].value
						city = flaArr[i].label
						break
					}
				}
				resolve({
					city: city,
					cityCode: cityCode,
					address: addressStr
				})
			}, function(e) {
				alert("Failed:" + JSON.stringify(e));
			});
		})
	},
	// 地址解析成经纬度
	addressToLatAndLng(address) {
		return new Promise((resolve, reject) => {
			plus.maps.Map.geocode("北京市朝阳区浦项中心", {}, function(event) {
				console.log('geocode', event);
				const location = {
					lat: event.coord.latitude,
					lng: event.coord.longitude
				}
				resolve(location)
			}, function(e) {
			});
		})
	}
}
