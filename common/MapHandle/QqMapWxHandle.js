import QQMapWX from './qqmap-wx-jssdk.min.js'
import config from '@/common/config.js'

export default {
	qqmapsdk: null,
	async load(id = 'qqmap_container') {
		const _this = this
		return new Promise((resolve, reject) => {
			_this.qqmapsdk = new QQMapWX({
				key: config.qqmapWxSdkKey
			})
			resolve(_this.qqmapsdk)
		})
	},
	getLocalCity() {
		const _this = this
		return new Promise((resolve, reject) => {
			wx.getLocation({
				type: 'wgs84',
				success (latLng) {
					_this.latAndLngToAddress(latLng.longitude, latLng.latitude).then((cityInfo) => {
						console.log('cityInfo', cityInfo);
						resolve(cityInfo.city)
					})
				}
			})
		})
	},
	// 获取精确位置的IP地址
	getCurrentPosition() {
		
	},
	// 经纬度解析成地址, 逆地理解析
	latAndLngToAddress(lng, lat) {
		return new Promise((resolve, reject) => {
			this.qqmapsdk.reverseGeocoder({
				location: {
					latitude: lat,
          			longitude: lng
				},
				success(resLocation) {
					if (resLocation.status == 0) {
						// console.log('latAndLngToAddress', resLocation.result);
						const addressObj = {
							address: resLocation.result.address_component,
							city: resLocation.result.address_component.city,
							country: resLocation.result.address_component.country,
							district: resLocation.result.address_component.district,
							province: resLocation.result.address_component.province,
							street: resLocation.result.address_component.street,
							streetNumber: resLocation.result.address_component.street_umber,
							town: resLocation.result.address_component.township
						}
						resolve(addressObj)
					}
					// resolve(resLocation.)
					// console.log('resLocation', resLocation);
				}
			})
		})
	},
	// 地址解析成经纬度, 地理解析
	addressToLatAndLng(addressStr) {
		return new Promise((resolve, reject) => {
			this.qqmapsdk.geocoder({
				address: addressStr,
				success(res) {
					console.log('addressToLatAndLng success', res);
					if (res.status == 0) {
						const location = {
							lat: res.result.location.lat,
							lng: res.result.location.lng
						}
						resolve(location)
					} else {
						resolve({})
					}
				},
				fail(err) {
					console.log('addressToLatAndLng fail', res);
				}
			})
		})
	}
}