

import config from '@/common/config.js'

export default {
	qqmap: null,
	load(id = 'qqmap_container') {
		const _this = this
		return new Promise((resolve, reject) => {
			var container = document.createElement('div');
			container.id = id
			container.style.display = 'none'
			document.body.appendChild(container);
			// //创建script标签
			var script = document.createElement("script");
			//设置标签的type属性
			script.type = "text/javascript";
			//设置标签的链接地址
			script.src = `https://map.qq.com/api/js?v=2.exp&key=${config.qqmapH5SdkKey}&callback=init`;
			//添加标签到dom
			document.body.appendChild(script);
			window.init = function () {
				console.log('window.init');
				var qqmap = new qq.maps.Map(document.getElementById(id), {})
				_this.qqmap = qqmap
				resolve(qqmap)
			}
			// var qqmap = new qq.maps.Map(document.getElementById(id), {})
			// _this.qqmap = qqmap
			// resolve(qqmap)
			// script.onload = function () {
			// 	console.log(qq);
			// 	setTimeout(() => {
			// 		var qqmap = new qq.maps.Map(document.getElementById(id), {})
			// 		_this.qqmap = qqmap
			// 		resolve(qqmap)
			// 	}, 2000)
			// }			
		})
	},
	getLocalCity() {
		return new Promise(async (resolve, reject) => {
			if (!this.qqmap) {
				await this.load()
			}
			const citylocation = new qq.maps.CityService({
				complete(result){
					// console.log('getLocalCity', result);
					resolve(result.detail.name)
				}
			});
			//调用searchLocalCity();方法    根据用户IP查询城市信息。
			citylocation.searchLocalCity();
		})
	},
	// 获取精确位置的IP地址
	getCurrentPosition() {
		
	},
	// 经纬度解析成地址, 逆地理解析
	latAndLngToAddress(lng, lat) {
		return new Promise(async (resolve, reject) => {
			if (!this.qqmap) {
				await this.load()
			}
			const geocoder = new qq.maps.Geocoder({
				complete(result){
					console.log('latAndLngToAddress', result)
					const addressObj = {
						address: result.detail.address,
						city: result.detail.addressComponents.city,
						country: result.detail.addressComponents.country,
						district: result.detail.addressComponents.district,
						province: result.detail.addressComponents.province,
						street: result.detail.addressComponents.street,
						streetNumber: result.detail.addressComponents.streetNumber,
						town: result.detail.addressComponents.town
					}
					resolve(addressObj)
				}
			})
			var latLng = new qq.maps.LatLng(lat, lng);
			geocoder.getAddress(latLng);
		})
	},
	// 地址解析成经纬度, 地理解析
	addressToLatAndLng(addressStr) {
		return new Promise(async (resolve, reject) => {
			if (!this.qqmap) {
				console.log('addressToLatAndLng and init qqmap');
				await this.load()
			}
			const geocoder = new qq.maps.Geocoder({
				complete(result){
					const location = {
						lat: result.detail.location.lat,
						lng: result.detail.location.lng
					}
					resolve(location)
				}
			})
			geocoder.getLocation(addressStr);
		})
	}
}