import AMapJS from "amap-js"
import config from '@/common/config.js'



export default {
	Amap: null,
	load(id = 'amp_container') {
		return new Promise((resolve, reject) => {
			// var aMapJSAPILoader = new AMapJS.AMapJSAPILoader({
			// 	key: "366fa30268423cbac363e421bba7c7b0",
			// 	v: "1.4.15", // 版本号
			// 	params: {
			// 		plugin: 'AMap.Geocoder,AMap.CitySearch,AMap.Geolocation'
			// 	}, // 请求参数
			// 	protocol: "https:" // 请求协议
			// })
			// aMapJSAPILoader.load().then(() => {
			// 	
			// })
			var container = document.createElement('div');
			container.id = id
			container.style.display = 'none'
			document.body.appendChild(container);
			//创建script标签
			var script = document.createElement("script");
			//设置标签的type属性
			script.type = "text/javascript";
			//设置标签的链接地址
			script.src = `https://webapi.amap.com/maps?v=1.4.15&key=${config.amapH5SdkKey}&plugin=AMap.Geocoder,AMap.CitySearch,AMap.Geolocation`;
			//添加标签到dom
			document.body.appendChild(script);
			
			script.onload = () => {
				this.Amap = new AMap.Map(id, {
					resizeEnable: true
				})
				resolve(this.Amap)
			}			
		})
	},
	getLocalCity() {
		return new Promise(async (resolve, reject) => {
			if (!this.Amap) {
				await this.load()
			}
			this.Amap.plugin('AMap.CitySearch', () => {
				const citySearch = new AMap.CitySearch()
				this.Amap.addControl(citySearch)
				citySearch.getLocalCity((status, result) => {
					console.log('getLocalCity', result)
					resolve(result.city)
				})
			})
			// aMapJSAPILoader.load().then((AMap) => {
			// 	if (!Amap) {
			// 		Amap = new AMap.Map("icneter", {
			// 			resizeEnable: true
			// 		})
			// 	}
			// 	this.Amap.plugin('AMap.CitySearch', () => {
			// 		const CitySearch = AMap.CitySearch
			// 		const citySearch = new CitySearch();
			// 		Amap.addControl(citySearch);
			// 		citySearch.getLocalCity((status, result) => {
			// 			console.log('getLocalCity', result)
			// 			resolve(result)
			// 		})
			// 	})
			// })
		})
	},
	// 获取精确位置的IP地址
	getCurrentPosition() {
		return new Promise(async (resolve, reject) => {
			if (!this.Amap) {
				await this.load()
			}
			this.Amap.plugin('AMap.Geolocation', () => {
				var geolocation = new AMap.Geolocation()
				this.Amap.addControl(geolocation);
				geolocation.getCurrentPosition((status, result) => {
					console.log('getCurrentPosition', result)
					resolve(result)
				})
			})
			// aMapJSAPILoader.load().then((AMap) => {
			// 	if (!Amap) {
			// 		Amap = new AMap.Map("icneter", {
			// 			resizeEnable: true
			// 		})
			// 	}
			// 	Amap.plugin('AMap.Geolocation', () => {
			// 		var geolocation = new AMap.Geolocation()
			// 		Amap.addControl(geolocation);
			// 		geolocation.getCurrentPosition((status, result) => {
			// 			console.log('getCurrentPosition', result)
			// 			resolve(result)
			// 		})
			// 	})
			// })
		})
	},
	// 经纬度解析成地址
	latAndLngToAddress(lng, lat) {
		return new Promise(async (resolve, reject) => {
			if (!this.Amap) {
				await this.load()
			}
			const lnglat = [lng, lat]
			
			const geocoder = new AMap.Geocoder({})
			geocoder.getAddress(lnglat, (status, result) => {
				console.log('latAndLngToAddress', status);
				console.log('latAndLngToAddress', result);
				if (status === 'complete') {
					const addressObj = {
						address: result.regeocode.formattedAddress,
						city: result.regeocode.addressComponent.city || result.regeocode.addressComponent.province,
						country: result.regeocode.addressComponent.country,
						district: result.regeocode.addressComponent.district,
						province: result.regeocode.addressComponent.province,
						street: result.regeocode.addressComponent.street,
						streetNumber: result.regeocode.addressComponent.streetNumber,
						town: result.regeocode.addressComponent.township
					}
					resolve(addressObj)
				} else {
					resolve(result)
				}
			});
			
// 			aMapJSAPILoader.load().then((AMap) => {
// 				if (!Amap) {
// 					Amap = new AMap.Map("icneter", {
// 						resizeEnable: true
// 					})
// 				}
// 
// 				if (!geocoder) {
// 					geocoder = new AMap.Geocoder({});
// 				}
// 				geocoder.getAddress(lnglat, function(status, result) {
// 					console.log('latAndLngToAddress', result);
// 					if (status === 'complete') {
// 						resolve(result.regeocode)
// 					} else {
// 						resolve(result)
// 					}
// 				});
// 			})
		})
	},
	// 地址解析成经纬度
	addressToLatAndLng(address) {
		return new Promise(async (resolve, reject) => {
			if (!this.Amap) {
				await this.load()
			}
			console.log('address', address);
			const geocoder = new AMap.Geocoder({})
			geocoder.getLocation(address, (status, result) => {
				console.log('addressToLatAndLng', status);
				console.log('addressToLatAndLng', result);
				if (status === 'complete') {
					const location = {
						lat: result.geocodes[0].location.lat,
						lng: result.geocodes[0].location.lng
					}
					resolve(location)
				} else {
					resolve({})
				}
			})
			
			
// 			aMapJSAPILoader.load().then((AMap) => {
// 				if (!Amap) {
// 					Amap = new AMap.Map("icneter", {
// 						resizeEnable: true
// 					})
// 				}
// 
// 				if (!geocoder) {
// 					geocoder = new AMap.Geocoder({});
// 				}
// 
// 				geocoder.getLocation(address, function(status, result) {
// 					if (status === 'complete') {
// 						resolve(result.geocodes[0].location)
// 					} else {
// 						resolve({})
// 					}
// 				});
// 			})
		})
	}

	// 	var geocoder,marker
	// 
	// 	function geoCode(data) {
	// 		if(!geocoder){
	// 			geocoder = new AMap.Geocoder({});
	// 		}
	// 		var address  = data
	// 		geocoder.getLocation(address, function(status, result) {
	// 			console.log(status);
	// 			console.log(result);
	// 			if (status === 'complete') {
	// 				uni.$emit('gotGeocoder', result.geocodes[0].location)
	// 			} else {
	// 				uni.$emit('gotGeocoder', {})
	// 			}
	// 
	// 			// 	var lnglat = result.geocodes[0].location
	// 			// 	if(!marker){
	// 			// 		marker = new AMap.Marker();
	// 			// 		Amap.add(marker);
	// 			// 	}
	// 			// 	marker.setPosition(lnglat);
	// 			// 	Amap.setFitView(marker);
	// 			// }else{
	// 			// 	log.error('根据地址查询位置失败');
	// 			// }
	// 		});
	// 	}


}
