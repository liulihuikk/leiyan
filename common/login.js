import store from '@/store/index.js'
import { getToken, loadInfo } from '@/common/request.js'


export const isLogin = () => {
	// 为了保证token有效, 每次判断是否登录时都要checkSession, Session的有效期是比token短的
	// 如果Session有效说明token就没过期, Session过期就要重新请求token, 保证token一直处于有效状态
	
	// console.log(store);
	
	// #ifdef APP-PLUS
	const userInfo = wx.getStorageSync('userInfo')
	const token = wx.getStorageSync('mt_token')
	const lastLoginTime = uni.getStorageSync('lastLoginTime')
	const nowTime = Date.now()
	const isTokenDeadline = (nowTime - lastLoginTime) > 5 * 24 * 60 * 60 * 1000
	if (isTokenDeadline) {
		uni.removeStorageSync('userInfo')
		uni.removeStorageSync('mt_token')
		return false
	}
	return userInfo && userInfo.id && token
	// #endif
	// #ifdef H5
	// H5直接判断cookie有无
	var reg = new RegExp("(^| )" + 'token' + "=([^;]*)(;|$)")
	return !!document.cookie.match(reg)
	// #endif
	
	// #ifdef MP-WEIXIN
	const userInfo = wx.getStorageSync('userInfo')
	const token = wx.getStorageSync('mt_token')
	const lastLoginTime = uni.getStorageSync('lastLoginTime')
	const nowTime = Date.now()
	const isTokenDeadline = (nowTime - lastLoginTime) > 5 * 24 * 60 * 60 * 1000
	if (isTokenDeadline) {
		uni.removeStorageSync('userInfo')
		uni.removeStorageSync('mt_token')
		return false
	}
	return userInfo && userInfo.id && token
	// #endif
}


export const getUserToken = () => {
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '加载中...'
		})
		wx.login({
			success(res) {
				getToken(res.code).then((tokenRes) => {
					uni.hideLoading()
					// 获取到token
					store.commit('setToken', tokenRes.result.token)
					if (tokenRes.result) {
						// wx.setStorageSync('userInfo', {id: tokenRes.userId})
						// loadInfo()
						// wx.setStorageSync('bindFlag', !tokenRes.unBindFlag)
						// wx.setStorageSync('mt_token', tokenRes.result.token)
						// wx.setStorageSync('tokenTimestamp', Date.now())
						wx.setStorageSync('userWxInfoCacheKey', tokenRes.result.userWxInfoCacheKey)
						wx.setStorageSync('lastCacheKeyTime', Date.now())
						resolve(true)
					} else {
						resolve(false)
					}
				})
			},
			fail(err) {
				uni.hideLoading()
				resolve(false)
			}
		})
	})
}


export const doLogin = async () => {
	const res = await new Promise((resolve, reject) => {
			// const userInfo = wx.getStorageSync('userInfo')
		wx.login({
			success(res) {
				getToken(res.code).then((tokenRes) => {
					// 获取到token
					console.log('wx.login getToken', tokenRes);
					store.commit('setToken', tokenRes.result.token)
					wx.setStorageSync('mt_token', tokenRes.result.token)
					if (tokenRes.result && tokenRes.result.userId) {
						store.commit('setLoginState', true)
						// 这里的登录状态说的是moti系统的登录
						// 获取到userId说明已经绑定过了, 直接登录, 后台会刷新token的有效期
						wx.setStorageSync('userInfo', {
							id: tokenRes.result.userId
						})
						wx.setStorageSync('tokenTimestamp', Date.now())
						resolve(true)
					} else {
						resolve(false)
					}
				})
			},
			fail(err) {
				console.log('login fail', err);
				resolve(false)
			}
		})
	})
}

export default {
	isLogin,
	doLogin,
	getUserToken
}