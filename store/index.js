import Vue from 'vue'
import Vuex from 'vuex'
import {
	stat
} from 'fs';

/*状态共享而非数据传递*/

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		token: '', // 小程序用户token
		tempData: null, // 页面临时传递的数据
		isLogin: false, // 当前是否登录状态
		cartNum: 0, // 购物车商品数量
		sendWay: {}, // 配送方式
		fromPage: '', // 页面来源标记
		formIds: [],
		city: '',
		isOneHour: ''
	},
	mutations: {
		// #ifdef MP-WEIXIN
		setToken(state, token) {
			state.token = token
		},
		// #endif
		clearTempData(state) {
			state.tempData = null
		},
		setTempData(state, data) {
			state.tempData = data
		},
		setLoginState(state, status) { // 设置用户是否登录状态
			state.isLogin = status
		},
		setCartNum(state, num) { // 设置用户购物车数量
			state.cartNum = num
		},
		setSendWay(state, type) {
			state.sendWay = type
		},
		setFromPage(state, page) {
			state.fromPage = page
		},
		clearFromPage(state) {
			state.fromPage = ''
		},
		addFormId(state, formId) {
			state.formIds.push(formId)
		},
		clearFormIds(state) {
			state.formIds = []
		},
		setCity(state, city) {
			state.city = city
		},
		setIsOneHour(state, bool) {
			state.isOneHour = bool
		}
	},
	getters: {
		// #ifdef MP-WEIXIN
		token(state) {
			if (!state.token) {
				const token = uni.getStorageSync('mt_token')
				state.token = token
			}
			return state.token
		},
		// #endif
		isLogin(state) {
			return state.isLogin
		},
		tempData(state) {
			return state.tempData
		},
		cartNum(state) {
			return state.cartNum
		},
		sendWay(state) {
			return state.sendWay
		},
		fromPage(state) {
			return state.fromPage
		},
		formIds(state) {
			return state.formIds
		},
		city(state) {
			return state.city
		},
		isOneHour(state) {
			return state.isOneHour
		}
	},
	actions: {
		
	}
})

export default store
