import Vue from 'vue'
import App from './App'

import store from './store/index.js'

import utils from './common/utils.js'
import { isLogin, doLogin, getUserToken } from '@/common/login.js'
import mutils from '@/utils/utils.js'
import Putils from '@/utils/UtilsPrivate.js'

import Mbtn from '@/components/mbtn/mbtn.vue'
import mMask from '@/components/mask/mask.vue'
import PopupCenter from '@/components/popupCenter/popupCenter.vue'
import PopupBottom from '@/components/popupBottom/popupBottom.vue'
import mmodal from '@/components/mmodal.vue'

// 微信埋点, 内部的代码只编译到微信小程序, 其他平台执行空函数, 兼容处理
// 后面可以把埋点操作都放到这一个文件里, 根据不通平台来编译
import wxBuryDot from '@/common/buryDot.js'


Vue.prototype.$store = store
Vue.prototype.$utils = utils
Vue.prototype.$mutils = mutils
Vue.prototype.$putils = Putils
Vue.prototype.$mstore = utils.mstore
Vue.prototype.$isLogin = isLogin
Vue.prototype.$wxBuryDot = wxBuryDot

// 非h5平台埋点指令执行一个空函数
Vue.directive('auto-pageview', ()=>{});
Vue.directive('track-event', ()=>{});
Vue.directive('track-pageview', ()=>{})


Vue.component('mbtn', Mbtn)
Vue.component('mmask', mMask)
Vue.component('popupcenter', PopupCenter)
Vue.component('popupbottom', PopupBottom)
Vue.component('mmodal', mmodal)

Vue.config.productionTip = false
App.mpType = 'app'

// 友盟统计
// #ifdef H5
import wechat from '@/static/h5/js/jssdkApi.js'
import uweb from 'vue-uweb'

Vue.prototype.$wechat = wechat
Vue.use(uweb,'1277890458')
// #endif


// #ifdef MP-WEIXIN
import FormIdLayout from '@/platforms/MP-WEIXIN/components/formIdLayout.vue'
Vue.prototype.$getUserToken = getUserToken
Vue.component('FormIdLayout', FormIdLayout)
// #endif


/**
 * H5单独需要的代码
 * 这里的埋点, 只有H5平台才会执行方法里的代码
 */
// #ifdef H5
import '@/static/h5/wcore.js'
// #endif
Vue.mixin({
	methods: {
		MonitorEvent(ec) {
			// #ifdef H5
			//  大数据埋点，事件 ec-事件名称
			console.log("触发MonitorEvent监控....")
			if (typeof WCore === 'undefined') return;

			var _core = new WCore();
			_core.options.cid = 'c_h5';
			var _user = new WCore.inputs.User();
			let userInfo = uni.getStorageSync("userInfo")
			_user.uid = userInfo ? userInfo.mobile : '#';
			var _pv = new WCore.inputs.PV(_user);
			var _event = new WCore.inputs.Event(_pv);
			_event.ec = ec;
			_event.ea = event ? event.type : "click";
			_core.send(_event);
			// #endif
		},
		MonitorPV() {
			// #ifdef H5
			//  大数据埋点，页面
			console.log("触发MonitorPV监控....")
			if (typeof WCore === 'undefined') return;
			var _core = new WCore();
			_core.options.cid = 'c_h5';
			var _user = new WCore.inputs.User();
			let userInfo = uni.getStorageSync("userInfo")
			_user.uid = userInfo ? userInfo.mobile : '#';
			var _pv = new WCore.inputs.PV(_user);
			_core.send(_pv);
			// #endif
		}
	}
})


/**
 * 小程序单独需要的代码
 */

// #ifdef MP-WEIXIN
import loginMask from '@/components/loginMask.vue'
Vue.component('loginMask', loginMask)
// #endif



const app = new Vue({
    ...App
})
app.$mount()
