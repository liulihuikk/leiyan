export default class LastMayday {
	palette(contents) {
		return ({
			width: '750rpx',
			height: '1334rpx',
			background: '#fff',
			views: [
				{
					type: 'image',
					url: contents[0],
					css: {
						left: 0,
						top: 0,
						width: '750rpx',
						height: '750rpx'
					},
				},
				{
					type: 'text',
					text: contents[1],
					css: {
						top: '815rpx',
						left: '30rpx',
						color: '#FA4650',
						fontSize: '32rpx'
					},
				},
				{
					type: 'text',
					text: contents[2],
					css: {
						top: '800rpx',
						left: '62rpx',
						color: '#FA4650',
						fontSize: '48rpx'
					},
				},
				{
					type: 'text',
					text: contents[3],
					css: {
						top: '810rpx',
						right: '25rpx',
						color: '#999999',
						fontSize: '24rpx'
					},
				},
				// {
				// 	type: 'text',
				// 	text: '热卖',
				// 	css: {
				// 		top: '850rpx',
				// 		left: '110rpx',
				// 		color: '#F94650',
				// 		fontSize: '24rpx',
				// 		background: '#FEECEE',
				// 		borderWidth: '2rpx',
				// 		borderColor: '#F94650',
				// 		borderRadius: '4rpx'
				// 	},
				// },
				// {
				// 	type: 'text',
				// 	text: '一小时达',
				// 	css: {
				// 		top: '850rpx',
				// 		left: '140rpx',
				// 		color: '#F94650',
				// 		fontSize: '24rpx',
				// 		background: '#FEECEE',
				// 		borderWidth: '2rpx',
				// 		borderColor: '#F94650',
				// 		borderRadius: '4rpx'
				// 	},
				// },
				{
					type: 'text',
					text: contents[4],
					css: {
						width: '700rpx',
						top: '890rpx',
						left: '30rpx',
						color: '#333333',
						fontSize: '36rpx',
						lineHeight: '50rpx'
					},
				},
				{
					type: 'text',
					text: contents[5],
					css: {
						top: '1025rpx',
						left: '32rpx',
						color: '#999999',
						fontSize: '26rpx',
					},
				},
				{
					type: 'text',
					text: contents[6],
					css: {
						top: '1025rpx',
						left: '162rpx',
						color: '#333333',
						fontSize: '26rpx',
					},
				},
				{
					type: 'image',
					url: contents[7],
					css: {
						width: '160rpx',
						height: '160rpx',
						top: '1122rpx',
						left: '32rpx',
					},
				},
				{
					type: 'text',
					text: contents[8],
					css: {
						top: '1156rpx',
						left: '224rpx',
						color: '#333333',
						fontSize: '36rpx',
					},
				},
				
				{
					type: 'image',
					url: contents[9],
					css: {
						width: '25rpx',
						height: '25rpx',
						top: '1215rpx',
						left: '224rpx',
					},
				},
				{
					type: 'text',
					text: contents[10],
					css: {
						top: '1215rpx',
						left: '255rpx',
						color: '#333333',
						fontSize: '20rpx',
					},
				},
				{
					type: 'image',
					url: contents[11],
					css: {
						width: '25rpx',
						height: '25rpx',
						top: '1215rpx',
						left: '347rpx',
					},
				},
				{
					type: 'text',
					text: contents[12],
					css: {
						top: '1215rpx',
						left: '377rpx',
						color: '#333333',
						fontSize: '20rpx',
					},
				},
				{
					type: 'image',
					url: contents[13],
					css: {
						width: '25rpx',
						height: '25rpx',
						top: '1215rpx',
						left: '473rpx',
					},
				},
				{
					type: 'text',
					text: contents[14],
					css: {
						top: '1215rpx',
						left: '506rpx',
						color: '#333333',
						fontSize: '20rpx',
					},
				},
				{
					type: 'image',
					url: contents[15],
					css: {
						width: '25rpx',
						height: '25rpx',
						top: '1215rpx',
						left: '600rpx',
					},
				},
				{
					type: 'text',
					text: contents[16],
					css: {
						top: '1215rpx',
						left: '630rpx',
						color: '#333333',
						fontSize: '20rpx',
					},
				},
			],
		});
	}
}

