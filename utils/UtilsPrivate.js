
export function getOrderStatus(isRefund, orderYn, orderStatus, distributionId, paramTime) {
	console.log(isRefund, orderYn, orderStatus, distributionId, paramTime);
	// 有order_status  10:待付款 20:待配送 25:部分发货 30:待收货 99:完成  
	// 也有：is_refund 是否退货（1：否，2：退货审核，3：退货完成）  也有order_yn：取消订单(1:否，2:是)'
	const obj = {
		status: '',
		text: ''
	}
	const _isRefund = parseInt(isRefund)
	const _orderYn = parseInt(orderYn)
	const _orderStatus = parseInt(orderStatus)
	const _distributionId = parseInt(distributionId)
	const _paramTime = paramTime


	const strArr = _paramTime && _paramTime.replace(/-|:/g, ' ').split(' ')
	let date = _paramTime ? new Date(strArr[0], strArr[1] - 1, strArr[2], strArr[3], strArr[4], strArr[5]).getTime() : +
		new Date()
	let timeStart = new Date().setHours(0, 0, 0, 0)
	let time9_30 = timeStart + 9.5 * 60 * 60 * 1000
	let time17_30 = timeStart + 17.5 * 60 * 60 * 1000

	let sendTypeText = ''

	if (_distributionId == 1) {
		const time = formatDate(date, 'HH:mm')
		const hourHalf = formatDate(date + 60 * 60 * 1000, 'HH:mm')
		if (date > time9_30 && date < time17_30) {
			sendTypeText = `${hourHalf}`
		} else {
			sendTypeText = '明日'
		}
	} else if (_distributionId == 2) {
		// const day = parseInt(formatDate(date, ''))
		sendTypeText = '明日送达'
	} else {
		const strArr = _paramTime && _paramTime.replace(/-|:/g, ' ').split(' ')
		let timeStamp = _paramTime ? new Date(strArr[0], strArr[1] - 1, strArr[2], strArr[3], strArr[4], strArr[5]).getTime() :
			+new Date()
		const after3 = timeStamp + 3 * 24 * 60 * 60 * 1000
		const dayAfter3 = formatDate(after3, 'MM月DD日')
		sendTypeText = `${dayAfter3}`
	}




	if (_isRefund === 3 || _isRefund === 2) {
		obj.status = _isRefund === 3 ? '已退货' : _isRefund === 2 ? '退货审核中' : '已完成'
		obj.text = _isRefund === 3 ? '' : _isRefund === 2 ? '' : ''
		obj.code = _isRefund

		return obj
	}

	if (_orderYn === 2) {
		obj.status = '已取消'
		obj.code = _orderYn
		return obj
	}

	obj.code = _orderStatus
	if (_distributionId == 3) {
		switch (_orderStatus) {
			case 10:
				obj.status = '待付款'
				break
			case 20:
				obj.status = '待发货'
				obj.text = `支付成功，订单即将发货。预计${sendTypeText}送达，请耐心等待`
				break
			case 25:
				obj.status = '部分发货'
				obj.text = `支付成功，订单即将发货。预计${sendTypeText}送达，请耐心等待`
				break
			case 30:
				obj.status = '待收货'
				obj.text = `商品正在发货中，预计${sendTypeText}送达，请耐心等待。`
				break
			case 99:
				obj.status = '已完成'
				obj.text = '订单已完成，感谢您的支持，MOTI到家平台的所有商品均为官方正品。'
				break
		}
	} else if (_distributionId == 2) {
		switch (_orderStatus) {
			case 10:
				obj.status = '待付款'
				break
			case 20:
				obj.status = '待发货'
				obj.text = '支付成功，订单即将发货。预计次日送达，请耐心等待'
				break
			case 25:
				obj.status = '待收货'
				obj.text = '商品正在配送，预计次日送达，请耐心等待'
				break
			case 30:
				obj.status = '待收货'
				obj.text = '商品正在配送，预计次日送达，请耐心等待'
				break
			case 99:
				obj.status = '订单已送达'
				obj.text = '订单已完成，感谢您的支持，MOTI到家平台的所有商品均为官方正品，您可以放心使用。'
				break
		}
	} else {
		switch (_orderStatus) {
			// 3-已被抢单 5-商家已商品校验 7-订单已推送到蜂鸟系统 9-系统拒单/配送异常 11-蜂鸟系统已接单 13-已分配骑手 15-骑手已到店 17-配送中 19-已送达 21-商品签收完成 23-抢单已取消 25-商品被拒签 27-商品申请退货中 29-商品退货退款完成
			case 10:
				obj.status = '待付款'
				break
			case 3:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 5:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 7:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 9:
				obj.status = '系统拒单/配送异常'
				obj.text = '系统拒单/配送异常'
				break
			case 11:
				obj.status = '商品打包中'
				obj.text = `预计${sendTypeText}送到您手中，MOTI认证商家火速打包中，商品全程验真，请您放心使用`
				break
			case 13:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 15:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 17:
				obj.status = '骑手配送中'
				obj.text = `预计${sendTypeText}送到您手中，您的订单正在配送，请耐心等待`
				break
			case 19:
				obj.status = '订单已送达'
				obj.text = '订单已送达。感谢您的支持，MOTI到家平台的所有商品均为官方正品。'
				break
			case 21:
				obj.status = '订单已完成'
				obj.text = '订单已送达，感谢您的支持，MOTI到家平台的所有商品均为官方正品，您可以放心使用。'
				break
			case 23:
				obj.status = '抢单已取消'
				obj.text = '抢单已取消'
				break
			case 25:
				obj.status = '商品被拒签'
				obj.text = '商品被拒签'
				break
			case 27:
				obj.status = '商品申请退货中'
				obj.text = '商品申请退货中'
				break
			case 29:
				obj.status = '商品退货退款完成'
				obj.text = '商品退货退款完成'
				break
		}
	}

	return obj
}


export default {
	goLogin(param = null) {
		/**
		 * @param {Object}  
		 */
		let str = ''
		if (param) {
			for (let item in param) {
				str = `&${item}=${param[item]}`
			}
			str = str.replace('&', '?')
		}
		
		uni.navigateTo({
			url: `/pages/login/login${str}`
		})
	}
}