
export default {
	checkMobile(mobile) {
		let reg = /^1[3456789]\d{9}$/
		return reg.test(mobile)
	},
	isWxBrowser() {
		let ua = navigator.userAgent.toLowerCase()
		return ua.match(/MicroMessenger/i) == 'micromessenger'
	}
}